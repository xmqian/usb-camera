package com.njzdr.usbcameratest10;

import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jiangdg.ausbc.MultiCameraClient;
import com.jiangdg.ausbc.base.CameraActivity;
import com.jiangdg.ausbc.callback.ICaptureCallBack;
import com.jiangdg.ausbc.camera.bean.CameraRequest;
import com.jiangdg.ausbc.render.env.RotateType;
import com.jiangdg.ausbc.widget.AspectRatioTextureView;
import com.jiangdg.ausbc.widget.IAspectRatio;
import com.njzdr.usbcameratest10.databinding.ActivityCameraTestBinding;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * Desc:
 *
 * @author coszero
 * Email:xmqian93@163.com
 * Date:2023/02/16 13:48
 */
public class CameraTestActivity extends CameraActivity {

    private ActivityCameraTestBinding binding;

    public static void startActivity(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, CameraTestActivity.class);
        context.startActivity(intent);
    }

    @Nullable
    @Override
    protected IAspectRatio getCameraView() {
        return new AspectRatioTextureView(this);
    }

    @Nullable
    @Override
    protected ViewGroup getCameraViewContainer() {
        return binding.flyContent;
    }

    @Nullable
    @Override
    protected View getRootView(@NotNull LayoutInflater layoutInflater) {
        binding = ActivityCameraTestBinding.inflate(layoutInflater);
        return binding.getRoot();
    }

    @Override
    protected void initData() {
        super.initData();

        captureVideoStart(new ICaptureCallBack() {
            @Override
            public void onBegin() {

            }

            @Override
            public void onError(@Nullable String s) {

            }

            @Override
            public void onComplete(@Nullable String s) {

            }
        },"",0);
    }

    @Nullable
    @Override
    protected UsbDevice getDefaultCamera() {
        List<UsbDevice> deviceList = getDeviceList();
        if (deviceList.size() > 0) {
            return deviceList.get(deviceList.size() - 1);
        }
        return null;
    }

    @NotNull
    @Override
    protected CameraRequest getCameraRequest() {
        return new CameraRequest.Builder()
                .setPreviewWidth(1280) // camera preview width
                .setPreviewHeight(720) // camera preview height
                .setRenderMode(CameraRequest.RenderMode.OPENGL) // camera render mode
                .setDefaultRotateType(RotateType.ANGLE_0) // rotate camera image when opengl mode
                .setAudioSource(CameraRequest.AudioSource.SOURCE_AUTO) // set audio source
                .setAspectRatioShow(true) // aspect render,default is true
                .setCaptureRawImage(true) // capture raw image picture when opengl mode
                .setRawPreviewData(false)  // preview raw image when opengl mode
                .create();
    }

    @Override
    public void onCameraState(@NotNull MultiCameraClient.ICamera iCamera, @NotNull State state, @Nullable String s) {
        switch (state) {
            case OPENED:

                break;
            case CLOSED:
                break;
            case ERROR:
                break;
        }
    }
}
