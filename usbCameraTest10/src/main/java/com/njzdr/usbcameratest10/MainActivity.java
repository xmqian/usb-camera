package com.njzdr.usbcameratest10;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.njzdr.usbcameratest10.databinding.ActivityMainBinding;

/**
 * Desc:
 *
 * @author coszero
 * Email:xmqian93@163.com
 * Date:2023/02/16 13:47
 */
public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        PermissionHelper.requestEasy(this, new PermissionHelper.CallBack() {
            @Override
            public void requestSuccess() {
                getSupportFragmentManager().beginTransaction().replace(R.id.fly_content, DemoCameraFragment.newInstance()).commit();
            }
        });
    }
}
