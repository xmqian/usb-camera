UVCCamera
=========
- [UVCCamera原始git库地址](https://github.com/saki4510t/UVCCamera)
- [UVCCamera镜像库地址](https://gitcode.net/mirrors/saki4510t/uvccamera?utm_source=csdn_github_accelerator)
- [UVCCamera库使用介绍](https://www.jianshu.com/p/972e05fa76a3)
- [基于Android设备获取USB外接摄像头的图像,写的不够详细，但有很多参考链接](https://blog.csdn.net/qq_34295800/article/details/122864164)

# 使用指南
- 需要修改so库生成架构,此文件修改 libuvccamera/src/main/jni/Application.mk
- 如果相机识别不出来,但可以在电脑上正常识别,那么在日志中全局搜索subclass将搜索到的subclass相关信息
(UsbHostManager: Added device UsbDevice[mName=/dev/bus/usb/007/004,mVendorId=1443,mProductId=37424,mClass=239,mSubclass=2)，
按照下面截图的格式，在xml 目录下的device_filter.xml中配置下

# Demo功能介绍
1）USBCameraTest0
            显示如何使用SurfaceView来启动/停止预览。

2）USBCameraTest
            显示如何启动/停止预览。这与USBCameraTest0几乎相同，
            但是使用自定义的TextureView来显示相机图像而不是使用SurfaceView。

3）USBCameraTest2
            演示如何使用MediaCodec编码器将UVC相机（无音频）的视频记录为.MP4文件。
            此示例需要API>=18，因为MediaMuxer仅支持API>=18。

4）USBCameraTest3
            演示如何将音频（来自内部麦克风）的视频（来自UVC相机）录制为.MP4文件。
            这也显示了几种捕捉静止图像的方式。此示例可能最适用于您的定制应用程序的基础项目。

5）USBCameraTest4
            显示了访问UVC相机并将视频图像保存到后台服务的方式。
            这是最复杂的示例之一，因为这需要使用AIDL的IPC。

6）USBCameraTest5
            和USBCameraTest3几乎相同，但使用IFrameCallback接口保存视频图像，
            而不是使用来自MediaCodec编码器的输入Surface。
            在大多数情况下，您不应使用IFrameCallback来保存图像，因为IFrameCallback比使用Surface要慢很多。
            但是，如果您想获取视频帧数据并自行处理它们或将它们作为字节缓冲区传递给其他外部库，
            则IFrameCallback将非常有用。

7）USBCameraTest6
            这显示了如何将视频图像分割为多个Surface。你可以在这个应用程序中看到视频图像并排观看。
            这个例子还展示了如何使用EGL来渲染图像。
            如果您想在添加视觉效果/滤镜效果后显示视频图像，则此示例可能会对您有所帮助。

8）USBCameraTest7
            这显示了如何使用两个摄像头并显示来自每个摄像头的视频图像。这仍然是实验性的，可能有一些问题。

9）usbCameraTest8
            这显示了如何设置/获取uvc控件。目前这只支持亮度和对比度。
            
10) usbCameraTest9
            使用打包后的uvcc库实现视频录制，拍照，获取视频流的功能
            
10) usbCameraTest10
            使用另一位大佬的在线依赖进行编写，实现拍照，录像，获取视频流功能
            
11) usbCameraTest11
            使用与test10一样的库,变化在于不使用在线依赖，仅将so库离线进行重新编写方法，增加了视频水印功能
