package com.serenegiant.usb.encoder;

/** 录制参数
 *
 * Created by jiangdongguo on 2017/10/19.
 */

public class RecordParams {
    private String recordPath;
    private int recordDuration;
    private boolean voiceClose;
    private boolean isAutoSave;
    private boolean isSupportOverlay;
    /**
     * 覆盖水印前缀
     */
    private String overlayTextPrefix;

    public RecordParams() {
    }

    public boolean isSupportOverlay() {
        return isSupportOverlay;
    }

    public void setSupportOverlay(boolean supportOverlay) {
        isSupportOverlay = supportOverlay;
    }

    public boolean isVoiceClose() {
        return voiceClose;
    }

    public void setVoiceClose(boolean voiceClose) {
        this.voiceClose = voiceClose;
    }

    public String getRecordPath() {
        return recordPath;
    }

    public void setRecordPath(String recordPath) {
        this.recordPath = recordPath;
    }

    public int getRecordDuration() {
        return recordDuration;
    }

    public void setRecordDuration(int recordDuration) {
        this.recordDuration = recordDuration;
    }

    public boolean isAutoSave() {
        return isAutoSave;
    }

    public void setAutoSave(boolean autoSave) {
        isAutoSave = autoSave;
    }

    public String getOverlayTextPrefix() {
        return overlayTextPrefix;
    }

    public void setOverlayTextPrefix(String overlayTextPrefix) {
        this.overlayTextPrefix = overlayTextPrefix;
    }
}
