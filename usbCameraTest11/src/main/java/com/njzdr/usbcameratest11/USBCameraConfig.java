package com.njzdr.usbcameratest11;

import android.os.Environment;

import java.io.File;

/**
 * Desc:
 *
 * @author coszero
 * Email:xmqian93@163.com
 * Date:2023/02/20 15:55
 */
public class USBCameraConfig {
    /**
     * 录制文件夹地址
     */
    public static String VIDEO_PATH_DIR = Environment.getExternalStorageDirectory().getAbsolutePath()
            + File.separator + "USBCamera" + File.separator + "videos" + File.separator;

    /**
     * 默认分辨率/宽
     */
    public static int DEFAULT_USB_CAM_WIDTH = 1280;
    /**
     * 默认分辨率/高
     */
    public static int DEFAULT_USB_CAM_HEIGHT = 720;

    /**
     * usb相机属性,用来查找指定相机
     * product-id="34374" vendor-id="9319"
     * - 产品Id 和 供应商id
     */
    public static int USB_CAMERA_PRODUCT_ID = 34374, USB_CAMERA_VENDOR_ID = 9319;
}
