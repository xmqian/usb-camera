package com.njzdr.usbcameratest11.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.njzdr.usbcameratest11.R;
import com.njzdr.usbcameratest11.databinding.ActivityUsbCameraDemoBinding;

/**
 * Desc:测试usb相机类，使用fragment控制相机
 *
 * @author coszero
 * Email:xmqian93@163.com
 * Date:2023/02/20 17:25
 */
public class USBCameraDemoActivity extends AppCompatActivity {

    private ActivityUsbCameraDemoBinding binding;

    public static void startActivity(Context context) {
            Intent intent = new Intent();
            intent.setClass(context,USBCameraDemoActivity.class);
            context.startActivity(intent);
        }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityUsbCameraDemoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getSupportFragmentManager().beginTransaction().replace(R.id.fly_content,USBCameraFragment.newInstance()).commit();
    }
}
